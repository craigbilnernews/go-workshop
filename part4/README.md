# Part 4 - bounded goroutines

It's actually quite expensive to spin up unlimited goroutines and
sometimes it may be beneficial to perform work on a limited number.
This magic number would be on a case by case basis for your scenario.

Also in the context of this being a lazy scheduler your application is
probably doing lots of other work and this little package shouldn't
hog *all* the resources.

## Goal

Assuming that you've created a goroutine for each piece of work try
benchmarking 1 million functions.

On my machine I get:

```bash
goos: linux
goarch: amd64
pkg: part3
BenchmarkScheduler_Run1m-8             1        1778118572 ns/op
```

* Make it faster

## Acceptance criteria

On my machine I get:

```bash
goos: linux
goarch: amd64
pkg: part4
BenchmarkScheduler_Run1m-8             2         751105232 ns/op
```

This uses a naïve solution that limits the number of goroutines to
100,000

## Learning outcomes

* runtime package
* Embedding
* Channels
