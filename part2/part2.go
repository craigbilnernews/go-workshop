package part2

type work func(...int) int

// We create the concept of a `job` here to link the work/schedule function, to it's arguments that it's scheduled with
type job struct {
	w    work
	args []int
}

// We put a private lists of jobs on it to retain the scheduled functions with their arguments
type Scheduler struct {
	jobs []job
}

func NewScheduler() *Scheduler {
	return &Scheduler{}
}

func (s *Scheduler) Add(w work, args ...int) {
	// The other predominant data structure is the slice, a dynamic array if you will
	// `append` will return a new slice, so don't forget to reassign the result
	// We also type `s` as a pointer to `Scheduler` so that we're mutating the same instance due to Go passing by value
	// If you use a pointer for one method it's idiomatic for all other methods to follow suit
	// We create a `job` and add it to our list of jobs, there's no value in making this a reference to a job
	// See https://golang.org/doc/effective_go.html#append for details
	s.jobs = append(s.jobs, job{w, args})
}

// We add a `Run` method which executes our scheduled work and returns the list of results
func (s *Scheduler) Run() []int {
	// `make` initialises slices, maps and channels.
	// Here we create a slice of `int` with a size and capacity equal to the number of jobs we have
	// this is a minor optimisation that allows us to just use the amount of memory required when creating the array
	// underneath
	// See https://golang.org/doc/effective_go.html#allocation_make for details
	results := make([]int, len(s.jobs))

	// The majority of your Go code will like be ranging over a data structure of some form
	// The benefit to `range` here is that we're interested in both the `index` and `value` of our `jobs`
	// We invoke the `work` on our `job` with the relevant `args` and assign it to the correct position in ours results
	// array
	for i, j := range s.jobs {
		results[i] = j.w(j.args...)
	}

	return results
}
