// Declare the name of the package which will house functionality with the same responsibility
// See https://golang.org/doc/effective_go.html#package-names for tips
package part1

// Anything that starts with a lowercase will be private to the package
// Titlecase will become public
// We alias `work` here for reuse
type work func(...int) int

// `struct` is the predominant data structure in Go where you manage state and hang methods off
// This is our main `struct` which we will attach our behaviours to
type Scheduler struct {
}

// Following idiomatic Go this returns a pointer to a Scheduler, we can add default here too if needed
// See https://golang.org/doc/effective_go.html#composite_literals for more details
func NewScheduler() *Scheduler {
	return &Scheduler{}
}

// We attach an `Add` method that takes a function and it's arguments which will satisfy our first requirement
// The `s *Scheduler` is a "pointer receiver", see https://golang.org/doc/effective_go.html#methods for details
// We can schedule a function and it's arguments and the function is no invoked
func (s *Scheduler) Add(w work, args ...int) {

}
