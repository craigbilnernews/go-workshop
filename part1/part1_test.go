// We add the _test suffix to make this an external test. You can now only use exposed package functionality
// rather than it's internals. This tends to make your tests based on behaviour rather than on implementation
package part1_test

// We can group imports with parenthesise (no commas) and also alias them
// The dot here exposes everything from our package
import (
	. "part1"
	"testing"
)

// Go has several conventions which must be followed and idioms which may or may not be followed
// Each test needs to start with `Test` in order for `go test` to pick it up but separating words by an underscore
// is an idiom. In this tutorial we will use the bare testing tools from the stdlib rather than a test framework
// You should explore the methods on `testing.T` to find what's appropriate for your test scenario
func TestScheduler_should_lazily_take_a_function_and_arguments(t *testing.T) {
	// Another Go idiom is to provide a `New` method to instantiate an "instance" of something
	// This allows you to setup defaults and adapt the underlying implementation in the future
	s := NewScheduler()

	// Our stub function just needs to satisfy the signature of things we can schedule
	// If it's called the test will fail with the given message
	f := func(...int) int {
		t.Fatalf("Scheduler is not lazy")
		return 0
	}

	// The API is completely down to you, here we can `Add` a function with it's arguments separately
	// You could also do:
	// s.Schedule(f, []int{1, 2})
	// s.Schedule(f, []int{1, 2}...) - this spreads the slice akin to calling it with separate params
	s.Add(f, 1, 2)
	s.Add(f, 1, 2)
}
