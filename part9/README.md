# Part 9 - with context

In Go you typically use `context.Context` to manage cancellations,
deadlines and timeouts. Assuming that you haven't done so
already now is the time to looking into how you would use
[context.WithTimeout](https://golang.org/src/context/context.go?s=14162:14239#L440)

## Goal

* Change your implementation to use `context.WithTimeout` to
finish in a given period of time to protect against long
operations

## Acceptance criteria

* Tests should pass

## Learning outcomes

* Context
