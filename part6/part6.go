package part6

import (
	"errors"
	"runtime"
	"time"
)

type work func(...int) int

type job struct {
	w    work
	args []int
}

type Scheduler struct {
	maxgr   int
	timeout time.Duration
	jobs    []job
}

// We now take in a duration for our timeout which we'll use to set as the ceiling for each piece of work
func NewScheduler(maxgr int, timeout time.Duration) *Scheduler {
	if maxgr == 0 {
		maxgr = runtime.NumCPU()
	}

	return &Scheduler{
		maxgr:   maxgr,
		timeout: timeout,
	}
}

func (s *Scheduler) Add(w work, args ...int) {
	s.jobs = append(s.jobs, job{w, args})
}

type worker struct {
	job
	position int
}

type workStream chan worker

// We now need a new type to store the overall result of racing our work against the timeout
type workResult struct {
	value,
	position int
	err error
}

type resultStream chan workResult

func doWork(ws workStream, rs resultStream, cancel <-chan struct{}, timeout time.Duration) {
	for {
		select {
		case worker := <-ws:
			// We now adapt our work case to run our work in it's own goroutine. This would still be capped because we
			// work serially within each `doWork` context but the total number will be 2x
			workStream := make(chan int, 1)

			go func() {
				workStream <- worker.w(worker.args...)
				// usually you want the owner of the channel (the writer) to close the channel to avoid nasty surprises
				close(workStream)
			}()

			select {
			case r := <-workStream:
				// If we managed to read from the work stream we can send the value with `nil` error
				rs <- workResult{
					r,
					worker.position,
					nil,
				}
			case <-time.After(timeout):
				// The `time.After` method gives us a channel that will close after the timeout and unblock the read
				// Being in the `select` will match on this one if the work couldn't be read in the first case
				// We then return the zero value for an `int` (0) and our custom `Timeout` error
				// We should really tell the work to not bother carrying on with another `Done` channel but for simplicity
				// we won't
				rs <- workResult{
					0,
					worker.position,
					Timeout,
				}
			}
		case <-cancel:
			return
		}
	}
}

// Our new `Result` type for consumers to interrogate as they see fit
type Result struct {
	Value int
	Err   error
}

// Our custom `Timeout` error that is publicly available for consumers to match on
var Timeout = errors.New("Timeout")

func (s *Scheduler) Run() []Result {
	totalWork := len(s.jobs)
	results := make([]Result, totalWork)
	workStreams := make([]workStream, 0)
	currentWorker := 0
	rs := make(resultStream, totalWork)
	cancelWorkers := make(chan struct{})

	defer close(rs)
	defer close(cancelWorkers)

	for i, j := range s.jobs {
		if len(workStreams) < s.maxgr {
			ws := make(workStream)
			workStreams = append(workStreams, ws)
			go doWork(ws, rs, cancelWorkers, s.timeout)
		}

		if currentWorker == len(workStreams) {
			currentWorker = 0
		}

		workStreams[currentWorker] <- worker{j, i}

		currentWorker++
	}

	for i := 0; i < totalWork; i++ {
		r := <-rs
		results[r.position] = Result{
			r.value,
			r.err,
		}
	}

	return results
}
