# Part 6 - provide a timeout

Currently our scheduled functions could take a very long time which will
impact every other result scheduled for the run.

## Goal

* Provide a timeout to the package so that we set a time ceiling to each
piece of work
* Return a sensible data structure for the consumers to determine what
to do with the result


## Acceptance criteria

* If you schedule a piece of work that takes longer than the timeout
value the consumer can see that it has timed out
* The work doesn't run for longer than the timeout value
* Other work is unaffected and returns the expeced result

## Learning outcomes

* handling timeouts
* errors package