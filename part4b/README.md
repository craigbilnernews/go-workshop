# Part 4 - bounded goroutines

This is an alternative solution which uses a buffered channel to manage
the maximum number of goroutines.

On my machine it's slightly faster:

```
goos: linux
goarch: amd64
pkg: part4b
BenchmarkScheduler_Run1m-8             2         710257002 ns/op
```

## Learning outcomes

* Buffered channels
* Channel blocking
